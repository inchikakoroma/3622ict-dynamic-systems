/*global flickrAuth */
/*global CryptoJS */
/*global flickr */
flickrAuth.SECRET = "fa09d2c269b31973";
flickrAuth.frob = "";

// Sign all flick request
flickrAuth.signRequest = function(url){
    
    // split the url into bits after the '?'
    var urlSplitBits = url.split('?');
    // we split all the methods
    var params = urlSplitBits[1].split("&");
    
    
    // we sort out all the parameters
    params = params.sort();
    
    for(var i = 0; i < params.length; i++){
        params[i] = params[i].replace("=", "");
       // console.log(params[i]);
    }
    
    // we joing everything
    var requestString = params.join("");
    
    // we put the secret infront of the request String
    requestString = flickrAuth.SECRET + requestString;
    
    // we hash the string using cryptoJS MD5
    var hash = CryptoJS.MD5(requestString);
    
    var signedRequest = url+"&api_sig="+hash;
    
    return signedRequest;
    
};

// flickr Login request
flickrAuth.signIn = function(){
    
    // get frob request
    var getFrob_URL = 'https://api.flickr.com/services/rest/?method=flickr.auth.getFrob&format=json&nojsoncallback=1'+'&'+flickr.flickrAPI_KEY;
    
    // we sign the request
    getFrob_URL = flickrAuth.signRequest(getFrob_URL);
    console.log(getFrob_URL);
    $.get(getFrob_URL, function(data){
        
        // flickr frob
        flickrAuth.frob = data.frob._content;
        console.log(flickrAuth.frob);
        
        // signin link for Flickr with FROB
        var authLink = "http://flickr.com/services/auth/?"+flickr.flickrAPI_KEY+"&perms=write&frob="+flickrAuth.frob;
        
        // we sign the request
        authLink = flickrAuth.signRequest(authLink);
        var test = authLink;
        console.log(test);
        
        // open a login window
        window.open(authLink);
        
        alert("Click to continue");
        console.log("user is logged in");
        //console.log(flickrAuth.frob);
        // we get the flickr token
        
        //flickrAuth.getToken();
    });
    
    
    
    
};

flickrAuth.getToken = function(){
  
  
    // get flickr Token
    var getToken_URL = 'https://api.flickr.com/services/rest/?method=flickr.auth.getToken&'+flickr.flickrAPI_KEY+'&frob='+flickrAuth.frob+'&format=json&nojsoncallback=1';
    
    // we sign the requset
    getToken_URL = flickrAuth.signRequest(getToken_URL);
    
    console.log(getToken_URL);
    
};


//http://api.flickr.com/services/rest/?method=flickr.auth.getToken&api_key={apiKey}&frob={frob}&format=json&nojsoncallback=1&api_sig={ApiSig} 