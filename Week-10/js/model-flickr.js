/*global flickr */
/* global flickrAuth */


// flickr API key
flickr.flickrAPI_KEY = 'api_key=9b853174c18ffb6f1b76caf3fe1fda95';

// flickr Images Array
flickr.flickrPhotos = [];

// number of all photos
flickr.flickrNumPhotos = 0;

// get sizes returned
flickr.flickrGetSizesReturned = 0;

// display flickr images
flickr.photoReadycallback;

// search flickr
flickr.flickrSearch = function(text, callback){
    
    // pass the display flickr to the function 
    flickr.photoReadycallback = callback;
    
    var searchText = "&searchText" + text ;
    
    // get the value of the text box
    var myStr = $("#textbox").val();
    
    // if the text box is empty we add a word to make the function work
    if(myStr == ""){
        alert("You must enter something to search");
       // myStr = "interesting";
    }else{
        
        // remove white space
        myStr.trim();
        
        // change string to lower case
        searchText = myStr.toLowerCase();
        
        // flickr Request URL
        var search_URL = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&text='+searchText+'&per_page=20&format=json&nojsoncallback=1'+ '&' +flickr.flickrAPI_KEY;
        
        // sign the request
        search_URL = flickrAuth.signRequest(search_URL);
        
        $.get(search_URL, function(photoData){
            console.log(photoData);
            flickr.getFLickPhotoLink(photoData);
        });
    
    }
    
    
};

 // get Flickr interesting photos function 
flickr.flickrInteresting = function(callback){
    
    // pass the display flickr to the function 
    flickr.photoReadycallback = callback;
    
    // flickr Request
    var interesting_URL = 'https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&per_page=20&format=json&nojsoncallback=1'+ '&' +flickr.flickrAPI_KEY;
    
    // sign the request URL
    interesting_URL = flickrAuth.signRequest(interesting_URL);
    
    $.get(interesting_URL, function(photoData){
        flickr.getFLickPhotoLink(photoData);
    });
};

// get Flickr images function
flickr.getFLickPhotoLink = function(photoData){
    
    // make the flickrPhotos array empty
    flickr.flickrPhotos = [];
    
    flickr.flickrNumPhotos = photoData.photos.photo.length;
    
    // loop to get all the photos in the array
    for (var i = 0; i < photoData.photos.photo.length; i++) {
       
        // make a photo object
        var photoObject = {id: photoData.photos.photo[i].id, description: photoData.photos.photo[i].title};
        
        // push the object to the empty array
        flickr.flickrPhotos.push(photoObject);
        // get the getFlickrImages function
        flickr.getFlickrImageSizes(photoObject);
        
        
    }
};

// get image sizes (change function name)
flickr.getFlickrImageSizes = function(photoObject){
    
        // get each photo id
       var photoIdS = photoObject.id;
       
       var small = 0;
       var large = 0;
       
        // flickr Request to get the sizes of all the photos
        var flickrSizes = 'https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&photo_id='+photoIdS+'&format=json&nojsoncallback=1'+ '&' +flickr.flickrAPI_KEY;
        
        // we call the sign request function
        flickrSizes = flickrAuth.signRequest(flickrSizes);
        
        $.get(flickrSizes, function(photoData){
            
            // get the small and Large images
            for (var x = 0; x < photoData.sizes.size.length; x++) {
                if(photoData.sizes.size[x].label == "Large"){
                    photoObject.lightbox = photoData.sizes.size[x].source;
                    large = 1;
                }
                if(photoData.sizes.size[x].label == "Small"){
                    photoObject.url = photoData.sizes.size[x].source;
                    //console.log("Small Label " + photoData.sizes.size[x].source);
                    small = 1;
                }
            }
            
            if(large == 0){
                // if no large image we get the last image 
                photoObject.lightbox = photoData.sizes.size[photoData.sizes.size.length - 1].source;
            }
            
            if(small == 0){
                // if no large image we get the 3rd image
                photoObject.url = photoData.sizes.size[2].source;
            }
            
            // count of all the images returned 
            flickr.flickrGetSizesReturned++;
            
            // when all images sizes are returned
            if(flickr.flickrGetSizesReturned == flickr.flickrNumPhotos){
              // view.displayImages(flickr.flickrPhotos); no longer needed
               // we reset the getSizesReturned 
               flickr.flickrGetSizesReturned = 0;
               console.log(flickr.flickrPhotos);
               flickr.photoReadycallback(flickr.flickrPhotos);
            }
            
        });
        
};
