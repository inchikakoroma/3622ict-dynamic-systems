
function sayHello(){
    alert("Feature is not available yet");
}

// flickr API key from GRIFFITH UNI
var flickrAPI_KEY = 'api_key=dc140afe3fd3a251c2fdf9dcd835be5c';

// flickr Images Array
var flickrPhotos = [];

// number of all photos
var flickrNumPhotos = 0;

// get sizes returned
var flickrGetSizesReturned = 0;

var searchText = " ";


// display images
function displayImages(){
        
        // get the thumbs by id
        var thumbs = $("#images");
    
        // create an empty string
        var htmlStr = "";
        
        for (var i = 0; i < flickrPhotos.length; i++){
           if(flickrPhotos.length != -1){
             
               htmlStr +=  '<figure><a href="'+flickrPhotos[i].lightbox+'" data-lightbox="myImages" data-title="'+flickrPhotos[i].id+'"><img src="'+flickrPhotos[i].url+'" alt="'+flickrPhotos[i].description+'" height="200" width="200"></img></a <figcaption>'+flickrPhotos[i].description+'</figcaption></figure>';
                
           }
           
        }
        
        thumbs.html(htmlStr);
        
       if(flickrPhotos.length == 0){
          //  console.log(flickrPhotos.length + " images displayed");   clean your code later
            while(flickrPhotos.length > 0){
                flickrPhotos.pop();
            }
        }
        
        while(flickrPhotos.length > 0){
                flickrPhotos.pop();
        }
        
        
        
    }
 

function flickerSearch(){
    
    // get the value of the text box
    var myStr = $("#textbox").val();
    
    
    
    // if the text box is empty we add a word to make the function work
    if(myStr == ""){
        alert("You must enter something to search");
       // myStr = "interesting";
    }else{
        // remove white space
        myStr.trim();
        // change string to lower case
        searchText = myStr.toLowerCase();
        
        var search_URL = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&text='+searchText+'&per_page=20&format=json&nojsoncallback=1'+ '&' +flickrAPI_KEY;
        
        $.get(search_URL, function(photoData){
            console.log(photoData);
            getFLickPhotoLink(photoData);
        });
        
        displayImages(flickrPhotos);
    }
    
    
}

 // get Flickr interesting photos function   
function flickrInteresting(){
    var interesting_URL = 'https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&per_page=20&format=json&nojsoncallback=1'+ '&' +flickrAPI_KEY;
    
    $.get(interesting_URL, function(photoData){
        getFLickPhotoLink(photoData);
    });
}

// get image sizes (change function name)
function getFLickPhotoLink(photoData){
    
    // make the flickrPhotos array empty
    flickrPhotos = [];
    
    flickrNumPhotos = photoData.photos.photo.length;
    
    // loop to get all the photos in the array
    for (var i = 0; i < photoData.photos.photo.length; i++) {
       
        // make a photo object
        var photoObject = {id: photoData.photos.photo[i].id, description: photoData.photos.photo[i].title};
        
        // push the object to the empty array
        flickrPhotos.push(photoObject);
        // get the getFlickrImages function
        getFlickrImageSizes(photoObject);
        
        
    }
}

// get Flickr images function
function getFlickrImageSizes(photoObject){
    
        // get each photo id
       var photoIdS = photoObject.id;
       
       var small = 0;
       var large = 0;
       
        // get the sizes of all the photos
        var flickrSizes = 'https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&photo_id='+photoIdS+'&format=json&nojsoncallback=1'+ '&' +flickrAPI_KEY;
        
        $.get(flickrSizes, function(photoData){
            
            // get the small and Large images
            for (var x = 0; x < photoData.sizes.size.length; x++) {
                if(photoData.sizes.size[x].label == "Large"){
                    photoObject.lightbox = photoData.sizes.size[x].source;
                    large = 1;
                   
                }
                if(photoData.sizes.size[x].label == "Small"){
                     photoObject.url = photoData.sizes.size[x].source;
                    //console.log("Small Label " + photoData.sizes.size[x].source);
                    small = 1;
                }
            
            }
            
            if(large == 0){
            
                       // if no large image we get the last image 
                    photoObject.lightbox = photoData.sizes.size[photoData.sizes.size.length - 1].source;
                    
            }
            
            if(small == 0){
                // if no large image we get the 3rd image
                    photoObject.url = photoData.sizes.size[2].source;
                    
            }
            
            // count of all the images returned 
            flickrGetSizesReturned++;
            
            // when all images sizes are returned
            if(flickrGetSizesReturned == flickrNumPhotos){
               displayImages(flickrPhotos);
               // we reset the getSizesReturned 
               flickrGetSizesReturned = 0;
            }
            
            
        });
        
}




$(document).ready(function(){
    
    // call get Flickr interesting photos
    flickrInteresting();
    
 
    // show thumbs event
    $("#enterButton").click(function(){
        $("#welcome").slideUp();
        $("#item").hide();
        $("#exit").show();
        $("#thumbs").slideDown(2000);
        
    });
    
    // close button event
    $("#closeButton").click(function() {
        $("#exit").hide();
        $("#thumbs").slideUp(500);
        $("#welcome").slideDown(1000);
        $("#item").show(1000);
        flickrInteresting();
    });
 
    // close button cursor
    $('#closeButton').css( 'cursor', 'pointer' );
    
    
    // search button event
    $("#searchButton").click(function(){
      flickerSearch();
    });

    // Register event handler
    $( "#textbox" ).keydown(function( event ) {
      if ( event.which == 13 ) {
        flickerSearch();
      }
  
    });
    
    
});



