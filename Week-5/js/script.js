
function sayHello(){
    alert("Feature is not available yet");
}

// empty array for found images
var found = [];

// search function
function search(photoData){
    
        // get the value of the text box
        var myStr = $("#textbox").val();
        // remove white space
        myStr.trim();
        // change string to lower case
        var str = myStr.toLowerCase();
        
        for(var i = 0; i < photoData.length; i++){
            if(photoData[i].description.toLowerCase().indexOf(str) != -1){
                    found.push(photoData[i]);
            }
        }
    
        displayImages(found);
    
}

// display images
function displayImages(){
    
        // create an empty string
        var htmlStr = "";
    
        for (var i = 0; i < found.length; i++){
           if(found.length != -1){
               var test = found.length;

               htmlStr +=  '<figure><a href="'+found[i].url+'" data-lightbox="myImages" data-title="City View"><img src="'+found[i].url+'" alt="'+found[i].description+'" height="200" width="200"></img></a <figcaption>'+found[i].description+'</figcaption></figure>';
           }
        }
        
        // get the thumbs by id
        var thumbs = $("#images");
        // make the thumbs equals to the empty htmlStr
        thumbs.html(htmlStr);
        
        // if no images display 
        if(found.length == 0){
            var message = "";
            message += '<h3 style="text-align: center; color: black";>' + "Invalid Search try again" + '</h3>';
            thumbs.html(message);
             while(found.length > 0){
                found.pop();
            }
        }

        // if the length is greater than 0 we empty the array
        while(found.length > 0){
            found.pop();
        }

    }


$(document).ready(function(){
 
    // show thumbs event
    $("#enterButton").click(function(){
        $("#welcome").slideUp();
        $("#item").hide();
        $("#exit").show();
        $("#thumbs").slideDown(2000);
    });
    
    // close button event
    $("#closeButton").click(function() {
        $("#exit").hide();
        $("#thumbs").slideUp(500);
        $("#welcome").slideDown(1000);
        $("#item").show(1000);
    });
 
    // close button cursor
    $('#closeButton').css( 'cursor', 'pointer' );
    
    
    // search button event
    $("#searchButton").click(function(){
        
        // fetch data
       $.get("data/photoData.json", function(data){
           search(data.data);
       }, "json");
    });

    // Register event handler
    $( "#textbox" ).keydown(function( event ) {
      if ( event.which == 13 ) {
        $.get("data/photoData.json", function(data){
           search(data.data);
       }, "json");
      }
  
    });
    
    
});



