// photo model
var photoData = [
    {
    url: "photos/DSC01049.JPG", 
    description: "City View",
    },
    
    {
    url: "photos/DSC01066.JPG", 
    description: "Ferris Wheel",
    },
    
    {
    url: "photos/DSC02511.jpg", 
    description: "Forbidden City",
    },
    
    {
    url: "photos/DSC03810.jpg", 
    description: "City View 2",
    },
    
    {
    url: "photos/DSC05750.jpg", 
    description: "Sunset",
    }

];

    // empty array for found images
  var found = [];
  
  

function search(){
    
    // get the value of the text box
    var myStr = document.getElementById("textbox").value;
    // remove white space
    myStr.trim();
    // change string to lower case
    var str = myStr.toLowerCase();
    
    for(var i = 0; i < photoData.length; i++){
        var mm = photoData[i];
        //console.log(mm);
        
        if(photoData[i].description.toLowerCase().indexOf(str) != -1){
                found.push(photoData[i]);
                
        }

    }
    
     
    
    displayImages(found);
    
}

// click button event
document.getElementById("searchButton").onclick = search;


//var enterPressed = document.getElementById("textbox");
//enterPressed.onkeydown = search;


function sayHello(){
    alert("Feature is not available yet");
}



function displayImages(){
    
    // create an empty string
    var htmlStr = "";
    
        for (var i = 0; i < found.length; i++){
           if(found.length != -1){
               var test = found.length;
               
               console.log(test + " found");
               htmlStr +=  '<figure><a href="'+found[i].url+'"><img src="'+found[i].url+'" alt="'+found[i].description+'" height="200" width="200"></img></a <figcaption>'+found[i].description+'</figcaption></figure>';
           }
        }
        
        // get the thumbs by id
        var thumbs = document.getElementById("images");
        // make the thumbs equals to the empty string
        thumbs.innerHTML = htmlStr;
        
        // if no images display 
        if(found.length == 0){
            var message = "";
            message += '<h3 style="text-align: center; color: black";>' + "Invalid Search try again" + '</h3>';
            thumbs.innerHTML = message;
             while(found.length > 0){
                found.pop();
            }
        }
      
    
    // if the length is greater than 0 we empty the array
    while(found.length > 0){
        found.pop();
    }

  //console.log(htmlStr);
}

// Register event handler
var textbox = document.getElementById("textbox");
textbox.onkeydown = printKey;

function printKey(event){
    
  if (event.keyCode == 13){
    search();
  }
}




