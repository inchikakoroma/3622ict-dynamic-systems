/* global facebookAuth */
/* global facebook */
/* global FB */
/* global view */


// display images
view.displayAlbums = function(photo){

        // get the thumbs by id
        var thumbs = $("#images");
        
        // get the page description by id
        var pageInfo = $("#pageInfo");
    
        // create an empty string
        var htmlStr = "";
        var pageStr = "";
        
        for (var i = 0; i < photo.length; i++){
                
           if(photo.length != -1){
             
               htmlStr +=  '<figure id="albums"><a href="#" onclick="facebook.getImages(\''+photo[i].id+'\'); view.hide()"><img src="'+photo[i].coverPhoto+'" alt="'+photo[i].description+'" height="121" width="150"></img></a <figcaption>'+photo[i].description+'</figcaption></figure>';
            
           }
           
        } 
        
     
        thumbs.html(htmlStr);
        
    };
    
// display pageinfo
view.displayPageInfo = function(data){
        
        // get the page description by id
        var pageInfo = $("#pageInfo");
        
        var pageStr = "";
         pageStr = data;
        
        // add the page description 
        pageStr = '<p>"'+pageStr +'"</p>';
        
        pageInfo.html(pageStr);
};

// display posts
view.displayPost = function(data){
       
        // get the page description by id
        var posts = $("#posts");
        
        // empty strings
        var pageStr = "";
        var idStr = "";
        
        for (var i = 0; i < data.length; i++){
                
           if(data.length != -1){
             
                // add the page description 
                pageStr += '<p><strong>"'+data[i].message+'"</strong></p>';
                pageStr += '<p>Created at: "'+data[i].date +'"</p></br>';
           }
           
        } 
        
        posts.html(pageStr, idStr);
};

// display images
view.displayImages = function(photos){
        
        // get the thumbs by id
        var thumbs = $("#images2");
        var images2_pannel = $("#images2_panel");
    
        // create an empty string
        var htmlStr = "";
        var htmlStr2 = "";
        
        
        for (var i = 0; i < photos.length; i++){
                htmlStr2 = '<strong>"'+photos[i].Album_name+'"</strong>';
                // if no images we set the name to the Album name
                if(!photos[i].name){
                     photos[i].name = photos[i].Album_name;  
                }
                // if no likes by me
                if(!photos[i].myLike){
                     photos[i].myLike = "";  
                }
                
                if(photos.length != -1){
                
                htmlStr +=  '<figure><a href="'+photos[i].lightbox+'" data-lightbox="myImages" data-title="'+photos[i].likes+' likes"><img src="'+photos[i].url+'" alt="'+photos[i].name+'" height="150" width="150"></img></a <figcaption>'+photos[i].name+'</figcaption><figcaption>'+photos[i].myLike+'</figcaption></figure>';
                
                }

        }
        
        thumbs.html(htmlStr);
        images2_pannel.html(htmlStr2);
        
    };
