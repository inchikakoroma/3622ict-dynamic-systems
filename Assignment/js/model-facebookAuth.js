/* global facebookAuth */
/* global facebook */
/* global FB */

// redirection URL 
facebookAuth.redirectionURL  = "https://2503ictweb-programming-inchikakoroma.c9.io/3622ict-dynamic-systems/Assignment/";

// access Token
facebookAuth.accessToken = "";

// facebook App Secret
facebookAuth.SECRET = "7232f3dd6f7d29441a440e888e9c296f";

// facebook user details
facebookAuth.userResponse = [];


 // facebook login function
facebookAuth.login = function(){
        
    FB.getLoginStatus(function(response) {
        
        // reload the the page when the user logs in
        window.location.reload();
        facebookAuth.accessToken = response.authResponse.accessToken;
        
    });
};



// facebook logout function
facebookAuth.logout = function(response){
    // replace the url 
    window.location = facebookAuth.redirectionURL;
    
    
};

// get user details
facebookAuth.getUserDetail = function(token){
    
    var url = "https://graph.facebook.com/v2.4/me?access_token="+token;
        
    $.get(url, function(data){
        // save the user's details to an array
        facebookAuth.userResponse = data;
        
        // split the string and get the first name of the user
        var splitUserName = facebookAuth.userResponse.name.split(' ');
        splitUserName = splitUserName[0];
        
        var pageStr = '<strong>Hi '+splitUserName +'</strong>';
        $("#userDetails").html(pageStr);
    });
    
};
