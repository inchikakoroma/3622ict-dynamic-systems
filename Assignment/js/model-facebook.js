/* global facebookAuth */
/* global facebook */
/* global FB */
/* global view */

// facebook APP ID
facebook.appID = "1647142002170067";

// DMS Travel Object ID
facebook.DmsID = "815157038515764";

// page info data for DMS Travel
facebook.pageInfo = [];

// all the facebook images count
facebook.numPhotos = 0;

// display flickr images
facebook.photoReadycallback;

// get sizes returned
facebook.facebookGetSizesReturned = 0;

// get the top Australian Destinations
facebook.topAusDestination = function(token, callback){
  
    // pass the display flickr to the function 
    facebook.photoReadycallback = callback;
  
    var ausDestination_URL = "https://graph.facebook.com/v2.5/"+facebook.DmsID+"?fields=description%2Calbums%7Blocation%2Cname%2Ccover_photo%2Cpicture%2Clikes%7D&access_token="+token;
    
    $.get(ausDestination_URL, function(photoData){
        facebook.getAlbums(photoData);
    });
    
};

// get the images with their ID's
facebook.getAlbums = function(photoData){
    
    // create an empty array for the destination images
    facebook.ausDestination = [];
    
    facebook.pageInfo = photoData.description;
    
    // loop to get all the photos in the array
    for (var i = 0; i < photoData.albums.data.length; i++) {
       
       // create a location variable
       var location  = photoData.albums.data[i].location;
      
       // if there is a location field
       if(location){
       
        // make a photo object
        var photoObject = {
            id: 
                photoData.albums.data[i].id, 
            description: 
                photoData.albums.data[i].name, 
            location: 
                photoData.albums.data[i].location, 
            coverPhoto: 
                photoData.albums.data[i].picture.data.url, 
            likes:
                photoData.albums.data[i].likes.data.length
            
        };
            
           // get all the Australian Albums
           if(location.indexOf("Australia") !== -1){
              // push the object to the empty array
            facebook.ausDestination.push(photoObject); 
           }
       }
       
       // sort the images by likes
        facebook.ausDestination.sort(function(a, b) {
            return parseFloat(b.likes) - parseFloat(a.likes);
        });
       
       view.displayAlbums(facebook.ausDestination);
       
    }
  
    view.displayPageInfo(facebook.pageInfo);
    
};

// get posts
facebook.getPosts = function(token){
    // posts array
    var posts = [];
    
    // post get url 
    var post_URL = "https://graph.facebook.com/v2.5/"+facebook.DmsID+"?fields=feed%7Blikes%2Cmessage%2Ccreated_time%2Cid%7D&access_token="+token;
    
    $.get(post_URL, function(post){
    
        // loop through the posts 
        for(var i = 0; i < post.feed.data.length; i++){
            // if there is a like
            var likes = post.feed.data[i].likes;
            if(likes){
                // loop through and get the post liked by the Admin
                for (var x = 0; x < post.feed.data[i].likes.data.length; x++){
                    if(post.feed.data[i].likes.data[x].id == facebook.DmsID){
                       
                        // create a new post object
                        var postObject = {
                            id: 
                                post.feed.data[i].id, 
                            message: 
                                post.feed.data[i].message, 
                            date: 
                                post.feed.data[i].created_time
                        };
                        
                        // push the object into the posts array
                        posts.push(postObject);
                    }
                }
            }
        }
        // display posts
        view.displayPost(posts)
    });
    
};

// get Images
facebook.getImages = function(albumId){
    
    FB.api(albumId,'GET',{"fields":"photos, name"},function(response){
      facebook.getPhotoLink(response);
    });
    
};

// get images link
facebook.getPhotoLink = function(photoData){
    
    // make the facebook Photos array empty
    facebook.photos = [];
    
    facebook.numPhotos = photoData.photos.data.length;
    
    // loop to get all the photos in the array
    for (var i = 0; i < photoData.photos.data.length; i++) {
        
        // make a photo object
        var photoObject = {id: photoData.photos.data[i].id, name: photoData.photos.data[i].name, Album_name: photoData.name};
        
         
        
        // push the object to the empty array
        facebook.photos.push(photoObject);
        // get the getImageSizes function
        facebook.getImageSizes(photoObject);
        
    }
    
    
};

// get image sizes

facebook.getImageSizes = function(photoObject){
    
    // get each photo id
    var photoIdS = photoObject.id;
       
    var small = 0;
    var large = 0;
    
    FB.api(photoIdS,'GET', {"fields":"name,images,likes"},function(response) {
        // get the small and Large images
        for (var x = 0; x < response.images.length; x++) {
            
            for(var y = 0; y < response.likes.data.length; y++){
                if(response.likes.data[y].id == facebookAuth.userResponse.id){
                    photoObject.myLike = "You liked this photo";
                }
            }
        
            if(response.images[x].height == "900"){
                photoObject.lightbox = response.images[x].source;
                large = 1;
                   
            }
            if(response.images[x].height == "320"){
                photoObject.url = response.images[x].source;
                small = 1;
                   
            }
            
        }
        
        if(large == 0){
            
            // sort the images
            response.images.sort(function(a, b) {
                return parseFloat(a.height) - parseFloat(b.height);
            });
            
            // if no large image we get the last image which will be the biggest one
            photoObject.lightbox = response.images[response.images.length - 1].source;
                    
        }
        
        if(small == 0){
            
             // sort the images
            response.images.sort(function(a, b) {
                return parseFloat(a.height) - parseFloat(b.height);
            });
            
            // if no large image we get the 3rd image
            photoObject.url = response.images[0].source;
                    
        }
        
        // like count
        photoObject.likes = response.likes.data.length;
        
        // count of all the images returned 
        facebook.facebookGetSizesReturned++;
        
        // when all images sizes are returned we display
        if(facebook.facebookGetSizesReturned == facebook.numPhotos){
            
               // we display the images
               view.displayImages(facebook.photos);
               // we reset the getSizesReturned count
               facebook.facebookGetSizesReturned = 0;
        }
      
    });
};


