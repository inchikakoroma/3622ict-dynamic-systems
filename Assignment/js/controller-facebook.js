/* global facebookAuth */
/* global facebook */
/* global FB */

var facebookAuth = {};
var facebook = {};
var view = {};

facebook.accessToken = "";

$(document).ready(function(){
    
// Initialize the SDK upon load
  FB.init({
    appId      : facebook.appID,
    status     : true,
    xfbml      : true,
    version    : 'v2.4' // or v2.0, v2.1, v2.2, v2.3
  });
  

  // listen for and handle login and logout events
  FB.Event.subscribe('auth.login', facebookAuth.login);
  FB.Event.subscribe('auth.logout', facebookAuth.logout);
  
  FB.getLoginStatus(function(response) {
      
    facebookAuth.accessToken = response.authResponse.accessToken;
    
    // get user's details
    facebookAuth.getUserDetail(response.authResponse.accessToken);
    
    // get top Aus Destinations
    facebook.topAusDestination(response.authResponse.accessToken);
    
    // get posts
    facebook.getPosts(response.authResponse.accessToken);
      
  });
  
    
    // events Handlers
    
    // show thumbs event
    $("#enterButton").click(function(){
        $("#welcome").slideUp();
        $("#item").slideUp();
        $("#thumbs").slideDown(2000);
        
    });
   
   // show the Album Images
    view.hide = function(){
        $("#images").slideUp();
        $("#pageInfo").slideUp();
        $("#images2_panel").slideDown(2000);
        $("#images2").slideDown(2000);
    };
    
    // show the Albums again
    view.show = function(){
        $("#images").slideDown(2000);
        $("#pageInfo").slideDown(2000);
        $("#images2").slideUp();
        $("#design").slideUp();
        $("#images2_panel").slideUp();
    };
    
    // show the Splash screen
    view.home = function(){
        $("#thumbs").slideUp(500);
        $("#welcome").slideDown(1000);
        $("#item").show(1000);
    };
    
    // show Design screen
    view.showDesign = function(){
        $("#images").slideUp();
        $("#images2_panel").slideUp();
        $("#images2").slideUp();
        $("#pageInfo").slideUp();
        $("#design").slideDown(2000);
    };
});