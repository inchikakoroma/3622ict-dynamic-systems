/*global controller */
/*global flickr */
/*global view */


// flickr API key from GRIFFITH UNI
flickr.flickrAPI_KEY = 'api_key=dc140afe3fd3a251c2fdf9dcd835be5c';

// flickr Images Array
flickr.flickrPhotos = [];

// number of all photos
flickr.flickrNumPhotos = 0;

// get sizes returned
flickr.flickrGetSizesReturned = 0;

// display flickr images
//flickr.displayFlickr;

// search flickr
flickr.flickerSearch = function(){
    
    // pass the display flickr to the function 
    //flickr.displayFlickr = displayFlickr;
    
    var searchText = "";
    
    // get the value of the text box
    var myStr = $("#textbox").val();
    
    // if the text box is empty we add a word to make the function work
    if(myStr == ""){
        alert("You must enter something to search");
       // myStr = "interesting";
    }else{
        // remove white space
        myStr.trim();
        // change string to lower case
        searchText = myStr.toLowerCase();
        console.log(searchText);
        var search_URL = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&text='+searchText+'&per_page=20&format=json&nojsoncallback=1'+ '&' +flickr.flickrAPI_KEY;
        
        $.get(search_URL, function(photoData){
            console.log(photoData);
            flickr.getFLickPhotoLink(photoData);
        });
        
        view.displayImages(flickr.flickrPhotos);
    }
    
    
};

 // get Flickr interesting photos function 
flickr.flickrInteresting = function(){
    
    // pass the display flickr to the function 
    //flickr.displayFlickr = displayFlickr;
    
    var interesting_URL = 'https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&per_page=20&format=json&nojsoncallback=1'+ '&' +flickr.flickrAPI_KEY;
    
    $.get(interesting_URL, function(photoData){
        flickr.getFLickPhotoLink(photoData);
    });
};

// get image sizes (change function name)
flickr.getFLickPhotoLink = function(photoData){
    
    // make the flickrPhotos array empty
    flickr.flickrPhotos = [];
    
    flickr.flickrNumPhotos = photoData.photos.photo.length;
    
    // loop to get all the photos in the array
    for (var i = 0; i < photoData.photos.photo.length; i++) {
       
        // make a photo object
        var photoObject = {id: photoData.photos.photo[i].id, description: photoData.photos.photo[i].title};
        
        // push the object to the empty array
        flickr.flickrPhotos.push(photoObject);
        // get the getFlickrImages function
        flickr.getFlickrImageSizes(photoObject);
        
        
    }
};

// get Flickr images function
flickr.getFlickrImageSizes = function(photoObject){
    
        // get each photo id
       var photoIdS = photoObject.id;
       
       var small = 0;
       var large = 0;
       
        // get the sizes of all the photos
        var flickrSizes = 'https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&photo_id='+photoIdS+'&format=json&nojsoncallback=1'+ '&' +flickr.flickrAPI_KEY;
        
        $.get(flickrSizes, function(photoData){
            
            // get the small and Large images
            for (var x = 0; x < photoData.sizes.size.length; x++) {
                if(photoData.sizes.size[x].label == "Large"){
                    photoObject.lightbox = photoData.sizes.size[x].source;
                    large = 1;
                   
                }
                if(photoData.sizes.size[x].label == "Small"){
                     photoObject.url = photoData.sizes.size[x].source;
                    //console.log("Small Label " + photoData.sizes.size[x].source);
                    small = 1;
                }
            
            }
            
            if(large == 0){
            
                       // if no large image we get the last image 
                    photoObject.lightbox = photoData.sizes.size[photoData.sizes.size.length - 1].source;
                    
            }
            
            if(small == 0){
                // if no large image we get the 3rd image
                    photoObject.url = photoData.sizes.size[2].source;
                    
            }
            
            // count of all the images returned 
            flickr.flickrGetSizesReturned++;
            
            // when all images sizes are returned
            if(flickr.flickrGetSizesReturned == flickr.flickrNumPhotos){
               view.displayImages(flickr.flickrPhotos);
               // we reset the getSizesReturned 
               flickr.flickrGetSizesReturned = 0;
            }
            
            
        });
        
};
