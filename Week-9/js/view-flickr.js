/*global controller */
/*global flickr */
/*global view */

// display images
view.displayImages = function(){
        
        // get the thumbs by id
        var thumbs = $("#images");
    
        // create an empty string
        var htmlStr = "";
        
        for (var i = 0; i < flickr.flickrPhotos.length; i++){
           if(flickr.flickrPhotos.length != -1){
             
               htmlStr +=  '<figure><a href="'+flickr.flickrPhotos[i].lightbox+'" data-lightbox="myImages" data-title="'+flickr.flickrPhotos[i].id+'"><img src="'+flickr.flickrPhotos[i].url+'" alt="'+flickr.flickrPhotos[i].description+'" height="200" width="200"></img></a <figcaption>'+flickr.flickrPhotos[i].description+'</figcaption></figure>';
                
           }
           
        }
        
        thumbs.html(htmlStr);
        
    };