/* global flickr */
/* global view */

// flickr photo model
flickr.photoData  = [
    {
    url: "photos/DSC01049.JPG", 
    description: "City View",
    },
    
    {
    url: "photos/DSC01066.JPG", 
    description: "Ferris Wheel",
    },
    
    {
    url: "photos/DSC02511.jpg", 
    description: "Forbidden City",
    },
    
    {
    url: "photos/DSC03810.jpg", 
    description: "City View 2",
    },
    
    {
    url: "photos/DSC05750.jpg", 
    description: "Sunset",
    }
];

flickr.found = [];

flickr.search = function(){
  
  var searchText = $("#searchText").val();
  // trim whitespaces
  
  if(searchText == ""){
    alert("You must enter something to search");
  }else{
    var mystr = searchText.toLowerCase();
    for(var i = 0; i < flickr.photoData.length; i++){
      if(flickr.photoData[i].description.toLowerCase().indexOf(mystr) != -1){
        flickr.found.push(flickr.photoData[i]);
      }
    }
    
    view.displayImages(flickr.found);
    console.log(flickr.found);
  }
  
};