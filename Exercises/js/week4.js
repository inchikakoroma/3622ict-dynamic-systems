$(function(){
  
  $("#myButton1").click(function(){
    $("#myDiv").html("I clicked a button");
    // using this selector
    $(this).val("Clicked");
  });
  
  $("#mytext").keydown(function(event){
      alert(event.which);
  });
  
  // jQuery Animation
  $("#div1").hide();
  
  $("#myButton").click(function(){
    $("#div1").toggle(500, function(){
      $("#div2").toggle(500);
    });
  });
  
  var params = {text: "dog",
                api_key: "dc140afe3"};
  var str = $.param(params);
  console.log(str);
  var url = 'http://api.flickr.com/services'+'?'+str;
  
  $.get(url, function(data){
    console.log(data);
  });

  
  
});