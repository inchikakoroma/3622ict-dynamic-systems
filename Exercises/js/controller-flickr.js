var flickr = {};
var controller = {};
var view = {};


$(document).ready(function(){
   //console.log("jQuery works"); 
   
   view.display();
   
   $("#searchButton").click(function(){
      flickr.search(); 
   });
   
   $("#images").hide();
   
   $("#loginButton").click(function(){
      $("#images").show();
   });
   
   $("#searchText").keydown(function(event){
      if(event.which == 13){
         flickr.search();
         
      }
   });
   
});