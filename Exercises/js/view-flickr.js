/* global view */
/* global flickr */


view.displayImages = function(){
    var thumbs = $("#thumbnails");
    
    var htmlStr = "";
    
    for(var i = 0; i < flickr.found.length; i++){
        if(flickr.found.length != -1){
            htmlStr +=  '<figure><a href="'+flickr.found[i].url+'" data-lightbox="myImages" data-title="'+flickr.found[i].description+'"><img src="'+flickr.found[i].url+'" alt="'+flickr.found[i].description+'" height="200" width="200"></img></a <figcaption>'+flickr.found[i].description+'</figcaption></figure>';
                
        } 
        
    }
    
    thumbs.html(htmlStr);
    console.log(flickr.found.length + "after loop");
    
     // if no images display 
        if(flickr.found.length == 0){
            var message = "";
            message += '<h3 style="text-align: center; color: black";>' + "Invalid Search try again" + '</h3>';
           
          thumbs.html(message);
        
        }

        // if the length is greater than 0 we empty the array
        while(flickr.found.length > 0){
            flickr.found.pop();
            console.log(flickr.found.length + "end of line");
        }
};

view.display = function(){
    
    var thumb = $("#thumbnails");
    
    var str = "";
    
    for(var x = 0; x < flickr.photoData.length; x++){
        console.log(flickr.photoData[x]);
    str += '<figure><a href="'+flickr.photoData[x].url+'" data-lightbox="myImages" data-title="'+flickr.photoData[x].description+'"><img src="'+flickr.photoData[x].url+'" alt="'+flickr.photoData[x].description+'" height="200" width="200"></img></a><figcaption>'+flickr.photoData[x].description+'</figcaption></figure>';
           
    }
    
    thumb.html(str);
    
};
