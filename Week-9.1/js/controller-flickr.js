/*global controller */
/*global flickr */
/*global angular */

var controll = {};
var flickr = {};

$(document).ready(function(){
 
 ///*** ASK why this does not work ***////
    // show thumbs event
    $("#enterButton").click(function(){
        
    });
    
    $("#loginButton").click(function(){
       sayHello(); 
    });
  
    
});

function sayHello(){
    alert("Option not yet avaliabl");
}

var app = angular.module('photoApp', ['ngRoute', 'ngAnimate']);

// angular routing 
app.config(function($routeProvider){
   
   $routeProvider
    .when('/', {
       templateUrl: 'pages/splash.html'
    })
    .when('/thumbs', {
        templateUrl: 'pages/thumbs.html',
        controller: 'photoReady'
    }); // end of statement
    
});

app.controller('photoReady', function($scope){
   
  
   // values for testing purposes only (remove later)
   //$scope.photos = [{
       //lightbox: "photos/DSC01049.JPG", 
      // url: "photos/DSC01049.JPG",
      // description: "this is description"
  // }];
   
   $scope.photoReady = function(photos){
       $scope.photos = photos;
       // this is needed to update changes when we go outside of Angular
       $scope.$apply();
   };
    
 
  flickr.flickrInteresting($scope.photoReady);
   
   // searchtext from the textbox
    var searchText = $scope.searchText;
   
   $scope.search = function(){
      
      flickr.flickrSearch(searchText, $scope.photoReady);
   };
   
   // enter button
   
   $( "#textbox" ).keydown(function( event ) {
      if ( event.which == 13 ) {
       flickr.flickrSearch(searchText, $scope.photoReady);
      }
  
    });
   
   
    
});

/*
app.controller('splash',['$scope', '$location', function($scope, $location){
    
    $scope.continue = function(){
        $location.path("/thumbs");
        $scope.apply();
    };
    
}]);

*/