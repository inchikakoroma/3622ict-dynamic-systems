var controller = {};
var flickr = {};
var view = {};

function sayHello(){
    alert("Feature is not available yet");
}

$(document).ready(function(){
    
    // call get Flickr interesting photos
    flickr.flickrInteresting(view.displayImages);
    
    // show thumbs event
    $("#enterButton").click(function(){
        $("#welcome").slideUp();
        $("#item").hide();
        $("#exit").show();
        $("#thumbs").slideDown(2000);
        
    });
    
    // close button event
    $("#closeButton").click(function() {
        $("#exit").hide();
        $("#thumbs").slideUp(500);
        $("#welcome").slideDown(1000);
        $("#item").show(1000);
        flickr.flickrInteresting(view.displayImages);
        
    });
 
    // close button cursor
    $('#closeButton').css( 'cursor', 'pointer' );
    
    
    // search button event
    $("#searchButton").click(function(){
      flickr.flickerSearch(controller.displayFlickr);
    });

    // Register event handler
    $( "#textbox" ).keydown(function(event) {
      if (event.which == 13 ) {
        flickr.flickerSearch(controller.displayFlickr);
      }
  
    });
    
});

/*
controller.displayFlickr = function(photos){
    view.displayImages(photos);
};
*/