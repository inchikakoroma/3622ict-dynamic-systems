
function sayHello(){
    alert("Feature is not available yet");
}

// flickr API key from GRIFFITH UNI
var flickrAPI_KEY = 'api_key=dc140afe3fd3a251c2fdf9dcd835be5c';

// flickr Images Array
var flickrPhotos = [];

var searchText = " ";

// empty array for found images
var found = [];

// search function
function flickerSearch(){
    
    // get the value of the text box
    var myStr = $("#textbox").val();
    // remove white space
    myStr.trim();
    // change string to lower case
    searchText = myStr.toLowerCase();
    
    var search_URL = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&text='+searchText+'&per_page=20&format=json&nojsoncallback=1'+ '&' +flickrAPI_KEY;
    
    $.get(search_URL, function(photoData){
        getFLickPhotoSizes(photoData);

      
    });
    
}

// display images
function displayImages(){
    
        // create an empty string
        var htmlStr = "";
    
        for (var i = 0; i < found.length; i++){
           if(found.length != -1){
               var test = found.length;

               htmlStr +=  '<figure><a href="'+found[i].url+'" data-lightbox="myImages" data-title="City View"><img src="'+found[i].url+'" alt="'+found[i].description+'" height="200" width="200"></img></a <figcaption>'+found[i].description+'</figcaption></figure>';
           }
        }
        
        // get the thumbs by id
        var thumbs = $("#images");
        // make the thumbs equals to the empty htmlStr
        thumbs.html(htmlStr);
        
        // if no images display 
        if(found.length == 0){
            var message = "";
            message += '<h3 style="text-align: center; color: black";>' + "Invalid Search try again" + '</h3>';
            thumbs.html(message);
             while(found.length > 0){
                found.pop();
            }
        }

        // if the length is greater than 0 we empty the array
        while(found.length > 0){
            found.pop();
        }

    }
 
// get Flickr interesting photos function   
function flickrInteresting(){
    var interesting_URL = 'https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&per_page=20&format=json&nojsoncallback=1'+ '&' +flickrAPI_KEY;
    
    $.get(interesting_URL, function(photoArray){
        //console.log(dataResults);
        getFLickPhotoSizes(photoArray);
    });
}

// get image sizes
function getFLickPhotoSizes(photoArray){
    
    // loop to get all the photos in the array
    for (var i = 0; i < photoArray.photos.photo.length; i++) {
        
        // get each photo id
        var photoIdS = photoArray.photos.photo[i].id;
       
        // get the sizes of all the photos
        var flickrSizes = 'https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&photo_id='+photoIdS+'&format=json&nojsoncallback=1'+ '&' +flickrAPI_KEY;
        
        
        $.get(flickrSizes, function(photoArray){
            var source = photoArray.sizes.size;
            console.log(source);
        });
    
    }
}


$(document).ready(function(){
    
    // call get Flickr interesting photos
    flickrInteresting();
 
    // show thumbs event
    $("#enterButton").click(function(){
        $("#welcome").slideUp();
        $("#item").hide();
        $("#exit").show();
        $("#thumbs").slideDown(2000);
    });
    
    // close button event
    $("#closeButton").click(function() {
        $("#exit").hide();
        $("#thumbs").slideUp(500);
        $("#welcome").slideDown(1000);
        $("#item").show(1000);
    });
 
    // close button cursor
    $('#closeButton').css( 'cursor', 'pointer' );
    
    
    // search button event
    $("#searchButton").click(function(){
        
      flickerSearch();
    });

    // Register event handler
    $( "#textbox" ).keydown(function( event ) {
      if ( event.which == 13 ) {
        flickerSearch();
      }
  
    });
    
    
});
